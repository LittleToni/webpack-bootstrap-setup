const MiniCssExtractPlugin      = require('mini-css-extract-plugin');
const { CleanWebpackPlugin }    = require('clean-webpack-plugin');
const HtmlWebpackPlugin         = require('html-webpack-plugin');
const path                      = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'assets'),
        filename: 'js/app.js',
    },
    devServer: {
        contentBase: path.join(__dirname, 'assets'),
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            config: {
                                path: './postcss.config.js',
                            },
                        },
                    },
                    'sass-loader',
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/app.css',
        }),
        new HtmlWebpackPlugin({
            title: 'Bootstrap Theme',
            meta: {
                'viewport': 'width=device-width, initial-scale=1, shrink-to-fit=no',
                'meta': 'charset=utf-8',
            },
            template: 'src/index.html',
            filename: 'index.html',
        }),
    ]
}