// Import jQuery
import 'jquery'

// Import bootstrap JS
import 'bootstrap'

// Import popper.js
import 'popper.js'

// Import SASS
import './scss/main.scss'
